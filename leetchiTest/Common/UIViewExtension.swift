//
//  UIViewExtension.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 24/02/2021.
//

import Foundation
import UIKit

protocol Drawable {
    var viewController: UIViewController? { get }
}

extension UIViewController: Drawable {
    var viewController: UIViewController? { return self }
}

extension UINavigationController {
    
    func customizeForRandom() {
        self.navigationBar.barTintColor = .black
        
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.yellow,
            NSAttributedString.Key.font: UIFont(name: "MarkerFelt-Wide", size: 24)!
        ]

        self.navigationBar.titleTextAttributes = attrs
    }
}

