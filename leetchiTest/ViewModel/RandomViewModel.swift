//
//  RandomViewModel.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 25/02/2021.
//

import Foundation
import RxSwift
import RxRelay

protocol  RandomViewModelProtocol : class {
    var gif: BehaviorRelay<Gif?> {get set}
    func getRandomGif()
}

class RandomViewModel:RandomViewModelProtocol {
    
    init(gif: Gif?) {
        self.gif = BehaviorRelay<Gif?>(value: gif)
    }
    
    var gif: BehaviorRelay = BehaviorRelay<Gif?>(value:nil)
    private let disposeBag = DisposeBag()
    
    func getRandomGif() {
        
        GifAPI.getRandomGif().subscribe(
            onNext: { element in
                self.gif.accept(element.gif)
            },
            onError: { error in
                print(error)
            }
        ).disposed(by: disposeBag)
    }
}
