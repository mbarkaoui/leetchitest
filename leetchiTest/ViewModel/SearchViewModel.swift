//
//  SearchViewModel.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 26/02/2021.
//

import Foundation
import RxSwift
import RxRelay
import RxDataSources

internal typealias isCompletion = (_ gif: Gif?) -> Void

protocol  SearchViewModelProtocol : class {
    var gifs: PublishSubject<[Gif]> { get }
    var gifsitems:[Gif]? {get set}
    var searchValue: PublishSubject<String> { get }
    var  didSelect: isCompletion? { get set }
    func getTrendingGif()
}

class SearchViewModel:SearchViewModelProtocol {
    var gifsitems: [Gif]?
    var didSelect: isCompletion?
    var searchValue: PublishSubject<String> =  PublishSubject()
    var gifs: PublishSubject<[Gif]> = PublishSubject()
    var collectionDataSource = RxCollectionViewSectionedAnimatedDataSource<SectionViewModel>(configureCell: {(_,_,_,_) in
        fatalError()
    },configureSupplementaryView: {(_,_,_,_) in
        fatalError()
    })
    
    private let disposeBag = DisposeBag()
    
    init() {
        searchValue.subscribe(onNext: { (value) in
            if value.isEmpty {
                self.getTrendingGif()
            } else {
                self.getSearchedGifs(query: value)
            }
        }).disposed(by: disposeBag)
    }
    
    func getSearchedGifs(query:String) {
        GifAPI.getSearchedGifs(query: query).subscribe(
            onNext: { element in
                self.gifs.onNext(element.gifs)
            },
            onError: { error in
                print(error)
            }
        ).disposed(by : disposeBag)
    }
    
    func getTrendingGif() {
        
        GifAPI.getTrendingGif().subscribe(
            onNext: { element in
                print("trending Gif")
                self.gifsitems = element.gifs
                self.gifs.onNext(element.gifs)
            },
            onError: { error in
                print(error)
            }
        ).disposed(by : disposeBag)
    }
}



