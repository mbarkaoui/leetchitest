//
//  SectionViewModel.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 27/02/2021.
//

import Foundation
import RxDataSources

struct SectionViewModel{
    var header: String
    var items: [Item]
}

extension SectionViewModel: AnimatableSectionModelType{
    var identity: UUID {
        return UUID()
    }
    
    typealias Item = Gif
    
    init(original: SectionViewModel, items: [Item]) {
        self = original
        self.items = items
    }
}
