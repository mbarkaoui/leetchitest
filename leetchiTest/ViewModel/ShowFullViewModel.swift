//
//  ShowFullViewModel.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 28/02/2021.
//

import Foundation
import RxSwift

protocol  ShowFullViewModelProtocol : class {
    var gif: Gif {get set}
}

class ShowFullViewModel:ShowFullViewModelProtocol {
    
    var gif: Gif
    
    init(gif: Gif) {
        self.gif = gif
    }
}

