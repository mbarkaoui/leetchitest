//
//  AppCoordinator.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 24/02/2021.
//

import Foundation
import UIKit

class AppCoordinator : BaseCoordinator {
    
    let window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
        super.init()
    }
    
    override func start() {
        
        // preparing root view
        let navigationController = UINavigationController()
        
        navigationController.customizeForRandom()
        let router = NavRouter(navigationController: navigationController)
        let showGifcoordinator = ShowGifCoordinator(router: router)
        
        // store child coordinator
        self.store(coordinator: showGifcoordinator)
        showGifcoordinator.start()
        
        router.pushContext(showGifcoordinator, isAnimated: true, onNavigateBack: { [weak self, weak showGifcoordinator] in
            guard let `self` = self, let myCoordinator = showGifcoordinator else { return }
            self.free(coordinator: myCoordinator)
        })
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
