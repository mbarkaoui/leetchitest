//
//  MainCoordinator.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 24/02/2021.
//

import Foundation
import UIKit

class ShowGifCoordinator: BaseCoordinator {
    
    lazy var myViewController: ShowGifViewController = {
        
        let viewController = ShowGifViewController.instantiate(storyboardName: "Main")
        viewController.modalPresentationStyle = .overFullScreen
        viewController.isModalInPresentation = true
        viewController.coordinator = self
        viewController.viewModel = RandomViewModel(gif: nil)
        
        return viewController
    }()
    
    init(router: RouterProtocol) {
        super.init()
        self.router = router
    }
    
    override func start() {
        // set viewmodel completion blocks in case we have intercation from View->viewModel->Coordinator
    }
    
    func presentSeachGifs() {
        
        let router = NavRouter(navigationController: UINavigationController())
        let searchCoordinator = SearchGifCoordinator(router: router)
        searchCoordinator.router = NavRouter(navigationController: UINavigationController(rootViewController: searchCoordinator.viewController ?? UIViewController() ))
        self.store(coordinator: searchCoordinator)
        searchCoordinator.start()
        
        self.router?.present(navigation: searchCoordinator.router?.navigationController ?? UINavigationController(), searchCoordinator, isAnimated: true, onNavigateBack: { [weak self, weak searchCoordinator] in
            guard let `self` = self, let myCoordinator = searchCoordinator else { return }
            self.free(coordinator: myCoordinator)
        })
    }
    
}

extension ShowGifCoordinator : Drawable {
    var viewController: UIViewController? { return myViewController }
}
