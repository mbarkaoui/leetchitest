//
//  searchCoordinator.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 24/02/2021.
//

import Foundation
import UIKit

class SearchGifCoordinator: BaseCoordinator {
    
    lazy var myViewController: SearchViewController = {
        
        let viewController = SearchViewController.instantiate(storyboardName: "Main")
        viewController.coordinator = self
        viewController.viewModel = SearchViewModel()
        return viewController
    }()
    
    init(router: RouterProtocol) {
        super.init()
        self.router = router
    }
    
    override func start() {
        // set viewmodel completion blocks in case we have intercation from View->viewModel->Coordinator
    }
    
    func showGifFullScreen(gif:Gif) {
        let viewController = ShowFullGifViewController.instantiate(storyboardName: "Main")
        viewController.coordinator = self
        viewController.viewModel = ShowFullViewModel(gif: gif)
        self.router?.pushView(viewController, isAnimated: true)
    }
}

extension SearchGifCoordinator : Drawable {
    var viewController: UIViewController? { return myViewController }
}
