//
//  NavRouter.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 24/02/2021.
//

import Foundation
import UIKit


typealias NavigationBackClosure = (() -> ())


typealias RouterProtocol = PushRouterProtocol & ModalRouterProtocol

protocol PushRouterProtocol: class {
    func pushContext(_ drawable: Drawable, isAnimated: Bool, onNavigateBack: NavigationBackClosure?)
    func pushView(_ view: UIViewController, isAnimated:Bool)
    func pop(_ isAnimated: Bool)
    func popToRoot(_ isAnimated: Bool)
    var navigationController:UINavigationController { get set}
}

protocol ModalRouterProtocol  {
    func present(navigation:UINavigationController,_ drawable: Drawable, isAnimated: Bool, onNavigateBack closure: NavigationBackClosure?)
    func dismiss(vc : UIViewController)
}


class NavRouter : NSObject, PushRouterProtocol, ModalRouterProtocol {
    
    public var navigationController: UINavigationController
    private var closures: [String: NavigationBackClosure] = [:]
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        super.init()
        self.navigationController.delegate = self
    }
    
    func pushView(_ view: UIViewController, isAnimated: Bool) {
        self.navigationController.pushViewController(view, animated: isAnimated)
    }
    
    func pushContext(_ drawable: Drawable, isAnimated: Bool, onNavigateBack closure: NavigationBackClosure?) {
        guard let viewController = drawable.viewController else {
            return
        }
        
        if let closure = closure {
            closures.updateValue(closure, forKey: viewController.description)
        }
        
        navigationController.pushViewController(viewController, animated: isAnimated)
    }
    
    func present(navigation:UINavigationController, _ drawable: Drawable, isAnimated: Bool ,onNavigateBack closure: NavigationBackClosure?) {
        
        guard let viewController = drawable.viewController else {
            return
        }
        
        if let closure = closure {
            closures.updateValue(closure, forKey: viewController.description)
        }
        
        navigationController.viewControllers[navigationController.viewControllers.count-1] .present(navigation, animated: isAnimated, completion: nil)
        
    }
    
    func dismiss(vc : UIViewController) {
        vc.dismiss(animated: false, completion: nil)
    }
    
    
    func pop(_ isAnimated: Bool) {
    }
    
    func popToRoot(_ isAnimated: Bool) {
    }
    
    public func executeClosure(_ viewController: UIViewController) {
        guard let closure = closures.removeValue(forKey: viewController.description) else { return }
        closure()
    }
}

extension NavRouter : UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        guard let previousController = navigationController.transitionCoordinator?.viewController(forKey: .from),
              !navigationController.viewControllers.contains(previousController) else {
            return
        }
        executeClosure(previousController)
    }
}
