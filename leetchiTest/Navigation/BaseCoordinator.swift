//
//  BaseCoordinator.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 24/02/2021.
//

import UIKit
import Foundation

class BaseCoordinator : Coordinator {
    
    var router: RouterProtocol?
    var childCoordinators : [Coordinator] = []
    var isCompleted: (() -> ())?
    
    func start() {
        fatalError("Children should implement `start`.")
    }
}
