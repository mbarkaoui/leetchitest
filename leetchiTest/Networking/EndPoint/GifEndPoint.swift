//
//  GifEndPoint.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation

public enum GifApi {
    case random
    case trending
    case searchedbyTitle(String)
}

extension GifApi: EndPointType {
    
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .production: return "https://api.giphy.com/v1/gifs"
        case .qa: return " https://api.giphy.com/v1/gifs"
        case .staging: return "https://api.giphy.com/v1/gifs"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        
        case .random:
            return "random"
            
        case .trending:
            return "trending"
            
        case .searchedbyTitle:
            return "search"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        
        case .random:
            return .get
            
        case .trending:
            return .get
            
        case .searchedbyTitle:
            return  .get
        }
        
    }
    
    var task: HTTPTask {
        switch self {
        case .searchedbyTitle(let q):
            
            return .requestParameters(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["api_key" : NetworkManager.APIKey,"q": q  ])
            
        default:
            return .requestParameters(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: ["api_key" : NetworkManager.APIKey])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}


