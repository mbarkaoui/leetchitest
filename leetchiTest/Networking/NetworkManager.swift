//
//  NetworkManager.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation

enum NetworkResponse:String,Error {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

class NetworkManager {
    static let environment : NetworkEnvironment = .production
    static let APIKey = "GL6rpQa23Cfkgmci1aaqQLRddAnnouvN"
    
}
