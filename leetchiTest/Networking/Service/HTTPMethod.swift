//
//  HTTPMethod.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}
