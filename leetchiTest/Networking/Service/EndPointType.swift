//
//  EndPointType.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
