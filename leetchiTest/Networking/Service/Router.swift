//
//  Router.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation
import RxSwift

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()


enum ResultType<Error>{
    case success
    case failure(Error)
}

protocol NetworkRouter {
    associatedtype EndPoint: EndPointType
//    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func request<T:Decodable>(_ route: EndPoint) -> Observable <T>
    func cancel()
}


class Router<EndPoint: EndPointType>: NetworkRouter {
 
    func request<T:Decodable>(_ route: EndPoint) -> Observable<T> {
        return Observable.create{ observer -> Disposable in
            let session = URLSession.shared
            do {
                let request = try self.buildRequest(from: route)
                NetworkLogger.log(request: request)
                self.task = session.dataTask(with: request, completionHandler: { data, response, error in
                    
                    if error != nil {
                        observer.onError(NetworkResponse.failed)
                       // completion(.failure(NetworkResponse.failed))
                    }
                    
                    if let response = response as? HTTPURLResponse {
                        let result = self.handleNetworkResponse(response)
                        switch result {
                        
                        case .success:
                            guard let responseData = data else {
                               // completion(.failure(NetworkResponse.noData))
                                return
                            }
                            do {
                                _ = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                               // print(jsonData)
                                let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
                                observer.onNext(apiResponse)
                            }catch {
                                observer.onError(NetworkResponse.unableToDecode)
                            }
                            
                        case .failure(let error):
                            observer.onError(error)
                            break
                            
                        }
                    }
                })
            }catch {
            }
            self.task?.resume()
            return Disposables.create{
                self.task?.cancel()
            }
        }
        
    }
    
    private var task: URLSessionTask?
//
//    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
//        let session = URLSession.shared
//        do {
//            let request = try self.buildRequest(from: route)
//            NetworkLogger.log(request: request)
//            task = session.dataTask(with: request, completionHandler: { data, response, error in
//                completion(data, response, error)
//            })
//        }catch {
//            completion(nil, nil, error)
//        }
//        self.task?.resume()
//    }
    
    func cancel() {
        self.task?.cancel()
    }
    
    fileprivate func buildRequest(from route: EndPoint) throws -> URLRequest {
        
        var request = URLRequest(url: route.baseURL.appendingPathComponent(route.path),
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 10.0)
        
        request.httpMethod = route.httpMethod.rawValue
        do {
            switch route.task {
            case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            case .requestParameters(let bodyParameters,
                                    let bodyEncoding,
                                    let urlParameters):
                
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
                
            case .requestParametersAndHeaders(let bodyParameters,
                                              let bodyEncoding,
                                              let urlParameters,
                                              let additionalHeaders):
                
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
            }
            return request
        } catch {
            throw error
        }
    }
    
    fileprivate func configureParameters(bodyParameters: Parameters?,
                                         bodyEncoding: ParameterEncoding,
                                         urlParameters: Parameters?,
                                         request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request,
                                    bodyParameters: bodyParameters, urlParameters: urlParameters)
        } catch {
            throw error
        }
    }
    
    fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
    
    public func handleNetworkResponse(_ response: HTTPURLResponse) -> ResultType<Error>{
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError)
        case 501...599: return .failure(NetworkResponse.badRequest)
        case 600: return .failure(NetworkResponse.outdated)
        default: return .failure(NetworkResponse.failed)
        }
    }
    
}
