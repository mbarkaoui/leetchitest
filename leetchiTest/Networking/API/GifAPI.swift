//
//  GifAPI.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation
import RxSwift

struct GifAPI {
    static let gifRouter = Router<GifApi>()
    
    static func getRandomGif() -> Observable<GifResponse>{
        gifRouter.request(.random)
    }
    
    static func getTrendingGif() -> Observable<GifsResponse> {
        gifRouter.request(.trending)
    }
    
    static func getSearchedGifs(query:String) -> Observable<GifsResponse> {
        gifRouter.request(.searchedbyTitle(query))
        
    }
    
    
    
    
    
}
