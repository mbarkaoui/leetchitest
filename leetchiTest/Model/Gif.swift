//
//  Gif.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation
import RxDataSources

struct Gif : Equatable,IdentifiableType {
    static func == (lhs: Gif, rhs: Gif) -> Bool {
        return lhs.id == rhs.id
    }
    
    typealias Identity = String
    var identity: Identity
    var id: String
    var title: String
    var images: Images
}

extension Gif: Decodable {
    
    enum UserCodingKeys: String, CodingKey {
        case id
        case title
        case images
    }
    
    init(from decoder: Decoder) throws {
        let userContainer = try decoder.container(keyedBy: UserCodingKeys.self)
        id = try userContainer.decode(String.self, forKey: .id)
        title = try userContainer.decode(String.self, forKey: .title)
        images = try userContainer.decode(Images.self, forKey: .images)
        identity = id
    }
}
