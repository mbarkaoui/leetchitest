//
//  GifsResponse.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 26/02/2021.
//

import Foundation

struct GifsResponse {
    let gifs: [Gif]
    let pagination: Pagination
}

extension GifsResponse: Decodable {
    
    private enum UserResponseCodingKeys: String, CodingKey {
        case gifs = "data"
        case pagination
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: UserResponseCodingKeys.self)
        gifs = try container.decode([Gif].self, forKey: .gifs)
        pagination = try container.decode(Pagination.self, forKey: .pagination)
    }
}
