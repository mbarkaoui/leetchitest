//
//  GifResponse.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 23/02/2021.
//

import Foundation

struct GifResponse {
    let gif: Gif
}

extension GifResponse: Decodable {
    
    private enum UserResponseCodingKeys: String, CodingKey {
        case gif = "data"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: UserResponseCodingKeys.self)
        gif = try container.decode(Gif.self, forKey: .gif)
    }
}
