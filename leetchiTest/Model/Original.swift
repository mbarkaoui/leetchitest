//
//  Original.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 26/02/2021.
//

import Foundation

struct GifInfo {
    let url: String
    let height : String
    let width : String
}

extension GifInfo: Decodable {
    
    enum OrignialCodingKeys: String, CodingKey {
        case url
        case height
        case width
    }
    
    init(from decoder: Decoder) throws {
        let originalContainer = try decoder.container(keyedBy: OrignialCodingKeys.self)
        url = try originalContainer.decode(String.self, forKey: .url)
        height = try originalContainer.decode(String.self, forKey: .height)
        width = try originalContainer.decode(String.self, forKey: .width)   
    }
}
