//
//  Pagination.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 26/02/2021.
//

import Foundation

struct Pagination {
    let offset: Int
    let totalCount: Int
    let count: Int
}

extension Pagination: Decodable {
    
    enum PaginationCodingKeys: String, CodingKey {
        case offset
        case totalCount = "total_count"
        case count
    }
    
    init() {
        offset = 0
        totalCount = 0
        count = 0
    }
    
    init(from decoder: Decoder) throws {
        let paginationContainer = try decoder.container(keyedBy: PaginationCodingKeys.self)
        
        offset = try paginationContainer.decode(Int.self, forKey: .offset)
        totalCount = try paginationContainer.decode(Int.self, forKey: .totalCount)
        count = try paginationContainer.decode(Int.self, forKey: .count)
        
    }
}


