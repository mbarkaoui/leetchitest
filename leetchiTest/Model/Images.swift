//
//  Images.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 26/02/2021.
//

import Foundation

struct Images {
    let original: GifInfo
    let fixedsmall : GifInfo
}

extension Images: Decodable {
    
    enum ImagesCodingKeys: String, CodingKey {
        case original
        case fixedsmall = "fixed_width_small_still"
    }
    
    init(from decoder: Decoder) throws {
        let imagesContainer = try decoder.container(keyedBy: ImagesCodingKeys.self)
        original = try imagesContainer.decode(GifInfo.self, forKey: .original)
        fixedsmall = try imagesContainer.decode(GifInfo.self, forKey: .fixedsmall)
        
    }
}
