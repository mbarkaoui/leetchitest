//
//  LTAnimatedImageView.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 28/02/2021.
//

import Foundation
import SDWebImage

class LTAnimatedImageView:SDAnimatedImageView {
    
    var imageLoadedCompletion: ((UIImage?,Error?,SDImageCacheType,URL?)->())?
    
    func setImageURL(imageURL:URL?){
        if let url  = imageURL {
            self.sd_setImage(with: url) { (image, error, imageCatchType, url) in
                if let completion = self.imageLoadedCompletion {
                    completion(image,error,imageCatchType,url)
                }
            }
        }
    }
}
