//
//  ShowFullGifViewController.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 28/02/2021.
//

import UIKit
import SDWebImage

class ShowFullGifViewController: UIViewController,Storyboarded {
    
    @IBOutlet weak var gifImageView: LTAnimatedImageView!
    weak var coordinator: SearchGifCoordinator?
    var viewModel: ShowFullViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let viewModel = self.viewModel else {
            return
        }
        setupUI(gif: viewModel.gif)
    }
    
    func setupUI(gif:Gif){
        gifImageView.setImageURL(imageURL: URL(string: gif.images.original.url))
    }
}
