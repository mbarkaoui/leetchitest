//
//  SecondViewController.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 24/02/2021.
//

import UIKit
import RxSwift
import RxRelay

class SearchViewController: UIViewController, Storyboarded{
    
    weak var coordinator: SearchGifCoordinator?
    var viewModel: SearchViewModel?
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var emptyResultLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    func setupUI(){
        self.title = "GiPHY"
        bindViewModel()
        setupCollectionView()
    }
    
    func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        let size = (collectionView.frame.size.width - CGFloat(30)) / CGFloat(3)
        flowLayout.itemSize = CGSize(width: size, height: size)
        collectionView.setCollectionViewLayout(flowLayout, animated: true)
    }
    
    func bindViewModel() {
        
        guard let viewModel = viewModel else {
            return
        }
        
        self.viewModel?.gifs
            .asObservable()
            .map({ (gifs) -> Bool in
                return !(gifs.isEmpty)
            })
            .bind(to: emptyResultLabel.rx.isHidden)
            .disposed(by:disposeBag)
        
        
        searchBar.rx.text
            .orEmpty
            .debounce(.seconds(1), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .debug()
            .bind(to: viewModel.searchValue)
            .disposed(by: disposeBag)
        
        collectionView
            .rx
            .modelSelected(Gif.self)
            .subscribe(onNext:{ item in
                print(item)
                self.coordinator?.showGifFullScreen(gif: item)
                //your code
            }).disposed(by: disposeBag)
        
        viewModel.collectionDataSource.configureCell = { (dataSource, collectionView, indexPath, item) in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectioncell", for: indexPath) as? GifCollectionViewCell
            cell?.configure(url: item.images.original.url)
            return cell!
        }
        
        viewModel.gifs
            .map({ [SectionViewModel(header:"Personal",items: $0) ]})
            .bind(to: collectionView.rx.items(dataSource: viewModel.collectionDataSource))
            .disposed(by: disposeBag)
    }
    
}
