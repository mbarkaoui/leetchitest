//
//  ViewController.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 22/02/2021.
//

import UIKit
import SDWebImage
import RxSwift
import RxRelay
import RxCocoa


class ShowGifViewController: UIViewController, Storyboarded {
    
    @IBOutlet weak var SearchGifsButton: UIButton!
    
    @IBOutlet weak var gifImageView: LTAnimatedImageView!
    weak var coordinator: ShowGifCoordinator?
    var viewModel: RandomViewModelProtocol?
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bindViewModel()
        self.setupBinding()
        self.setupUI()
    }
    
    func setupBinding(){
        SearchGifsButton.rx.tap.asObservable().bind {
        self.coordinator?.presentSeachGifs()
        }.disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showRandomGif()
        print("showrandom")
    }
    
    func setupUI() {
        self.title = "Giphy"
        SearchGifsButton.layer.cornerRadius = 20
        SearchGifsButton.layer.borderWidth = 2
        SearchGifsButton.layer.borderColor = UIColor.black.cgColor
        SearchGifsButton.layer.backgroundColor = UIColor.black.cgColor
    }
    
    func bindViewModel() {
        
        viewModel?.gif.asObservable().subscribe(onNext: { (gif) in
            if let gif = gif {
                
                self.gifImageView.setImageURL(imageURL: URL(string: gif.images.original.url))
            }
        }).disposed(by: disposeBag)
    }
    
    func showRandomGif() {
        self.viewModel?.getRandomGif()
    }  
}
