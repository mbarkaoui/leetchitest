//
//  GifCollectionViewCell.swift
//  leetchiTest
//
//  Created by Malek BARKAOUI on 26/02/2021.
//

import UIKit
import SDWebImage

class GifCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var GifImageView: SDAnimatedImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(url:String) {
        self.GifImageView.sd_setImage(with: URL(string: url)!, completed: nil)
        GifImageView.sd_setImage(with: URL(string: url), completed: nil)
    }
}
