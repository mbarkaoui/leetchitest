//
//  leetchiTestTests.swift
//  leetchiTestTests
//
//  Created by Malek BARKAOUI on 22/02/2021.
//

import XCTest
import RxSwift
import RxCocoa

@testable import leetchiTest

class RandomViewModelMock:RandomViewModelProtocol {
    var gif: BehaviorRelay<Gif?> = BehaviorRelay(value: nil)
    
    func getRandomGif() {
        self.gif.accept(Gif(identity: "eLcrZBNVJTiqkNgJjI", id: "eLcrZBNVJTiqkNgJjI", title: "Malek Intriga GIF by Clandestina", images: Images(original: GifInfo(url: "https://media3.giphy.com/media/eLcrZBNVJTiqkNgJjI/giphy.gif?cid=76b5ebfc8a9es1o5sqkicg5xt2hc73dhwvfpam1auflogvbl&rid=giphy.gif", height: "480", width: "480"), fixedsmall: GifInfo(url: "https://media3.giphy.com/media/eLcrZBNVJTiqkNgJjI/giphy.gif?cid=76b5ebfc8a9es1o5sqkicg5xt2hc73dhwvfpam1auflogvbl&rid=giphy.gif", height: "100", width: "100"))))
    }
}

class ShowGifViewControllerTests: XCTestCase {

    func test_WhenImageViewHaveData(){
        let sut = makeSUT()
        
        guard let viewModel = sut.viewModel else {
            return
        }
        
        let  exp  = expectation(description: "loading Gif")
        sut.gifImageView.imageLoadedCompletion = {_,_,error,_ in
            XCTAssertNil(error)
            print("loool")
            exp.fulfill()
        }
      
        
        waitForExpectations(timeout: 30)
    }
    
    func makeSUT() -> ShowGifViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sut = storyboard.instantiateViewController(identifier: "ShowGifViewController") as! ShowGifViewController
        let viewModelMock = RandomViewModelMock()
        sut.viewModel = viewModelMock
        sut.loadViewIfNeeded()
        _ = sut.view
        return sut
    }
    
}



